// Импортируем jQuery
// = ../../../node_modules/jquery/dist/jquery.js

// Импортируем Popper
// = ../../../node_modules/popper.js/dist/umd/popper.js

// Импортируем необходимые js-файлы Bootstrap 4
// = ../../../node_modules/bootstrap/js/dist/util.js
// = ../../../node_modules/bootstrap/js/dist/alert.js
// = ../../../node_modules/bootstrap/js/dist/button.js
// = ../../../node_modules/bootstrap/js/dist/carousel.js
// = ../../../node_modules/bootstrap/js/dist/collapse.js
// = ../../../node_modules/bootstrap/js/dist/dropdown.js
// = ../../../node_modules/bootstrap/js/dist/modal.js
// = ../../../node_modules/bootstrap/js/dist/tooltip.js
// = ../../../node_modules/bootstrap/js/dist/popover.js
// = ../../../node_modules/bootstrap/js/dist/scrollspy.js
// = ../../../node_modules/bootstrap/js/dist/tab.js
// = ../../../node_modules/bootstrap/js/dist/toast.js

// Импортируем другие js-файлы
// = app.js


// Accordion desktop //

const shop__button = document.querySelector('.shop-accordion__button');
const education__button = document.querySelector('.education-accordion__button');
const shop__accordion = document.querySelector('.shop__accordion');
const education__accordion = document.querySelector('.education__accordion');

shop__button.addEventListener('mouseenter', () => {
    shop__accordion.classList.toggle('shop__accordion--active');
    education__accordion.classList.remove('education__accordion--active');
})

education__button.addEventListener('mouseenter', () => {
    education__accordion.classList.toggle('education__accordion--active');
    shop__accordion.classList.remove('shop__accordion--active');
})

// Accordion mobile //

var acc = document.getElementsByClassName("burger-accordion__button");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("burger-accordion__button--active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}

// Burger mobile menu //

const burger__active = document.querySelector('.header-mobile__button');
const burger__close = document.querySelector('.header-burger__close');
const burger__menu = document.querySelector('.header-burger__menu');

burger__active.addEventListener('click', () => {
    burger__menu.classList.toggle('header-burger__menu--active');
	burger__active.classList.toggle('header-mobile__button--delete');
})

burger__close.addEventListener('click', () => {
    burger__menu.classList.remove('header-burger__menu--active');
	burger__active.classList.remove('header-mobile__button--delete');
})


// Slick Slider Main Page //

$('.review__slider').slick(
  {
    arrows: true,
    adaptiveHeight: true,
    nextArrow:"<button type='button' class='slick-next'></button>",
    prevArrow:"<button type='button' class='slick-prev'></button>",
    fade: true,
    cssEase: 'linear'
  }
);

// Show & Hide Text //


// Form Setting //

;(function() {
	'use strict';

	class Form {
		// паттерны RegExp о которых было написано выше
		static patternName = /^[а-яёА-ЯЁ\s]+$/;
		static patternMail = /^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z])+$/;
    	static patternPhone = /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/;
		// массив с сообщениями об ошибке
		// эти сообщения можно разместить и внутри кода валидации, но лучше,
		// если они будут находиться в одном месте
		// это облегчит их редактирование, а так же проще будет прописать новые,
		// если решите добавить критерии валидации
		static errorMess = [
			'Незаполненное поле ввода', // 0
			'Введите Ваше реальное имя', // 1
			'Укажите Вашу электронную почту', // 2
			'Неверный формат электронной почты', // 3
      		'Введите Ваш номер телефона', // 4
      		'Неверный формат номера телефона', // 5
			'Напишите текст сообщения' // 6
		];

		constructor(form) {
			this.form = form;
			// коллекция полей формы из которой мы будем извлекать данные
			this.fields = this.form.querySelectorAll('.form-control');
			// объект кнопки, на который повесим обработчик события начала валидации формы
			// и отправки её значений на сервер
			this.btn = this.form.querySelector('[type=submit]');
			// флаг ошибки валидации
			this.iserror = false;
			// регистрируем обработчики событий
			this.registerEventsHandler();
		}

		static getElement(el) {
			// получение элемента, в который будет выводиться
			// текст ошибки
			return el.parentElement.nextElementSibling;
		}

		registerEventsHandler() {
			// запуск валидации при отправке формы
			this.btn.addEventListener('click', this.validForm.bind(this));
			// очистка ошибок при фокусе поля ввода
			this.form.addEventListener('focus', () => {
				// находим активный элемент формы
				const el = document.activeElement;
				// если этот элемент не <button type="submit">,
				// вызываем функцию очистки <span class="error"> от текста ошибки
				if (el === this.btn) return;
				this.cleanError(el);
			}, true);
			// запуск валидации поля ввода при потере им фокуса
			for (let field of this.fields) {
				field.addEventListener('blur', this.validBlurField.bind(this));
			}
		}

		validForm(e) {
			// отменяем действие браузера по умолчания при клике по
			// кнопке формы <button type="submit">, чтобы не происходило обновление страницы
			e.preventDefault();
			// объект представляющий данные HTML формы
			const formData = new FormData(this.form);
			// объявим переменную error, в которую будем записывать текст ошибки
			let error;

			// перебираем свойства объекта с данными формы
			for (let property of formData.keys()) {
				// вызываем функцию, которая будет сравнивать 
				// значение свойства с паттерном RegExp и возвращать результат
				// сравнения в виде пустой строки или текста ошибки валидации
				error = this.getError(formData, property);
				if (error.length == 0) continue;
				// устанавливаем флаг наличия ошибки валидации
				this.iserror = true;
				// выводим сообщение об ошибке
				this.showError(property, error);
			}

			if (this.iserror) return;
			// вызываем функцию отправляющую данные формы,
			// хранящиеся в объекте formData, на сервер
			this.sendFormData(formData);
		}

		validBlurField(e) {
			const target = e.target;
			// имя поля ввода потерявшего фокус 
			const property = target.getAttribute('name');
			// значение поля ввода
			const value = target.value;

			// создаём пустой объект и записываем в него
			// данные в формате 'имя_поля': 'значение', полученные
			// от поля ввода потерявшего фокус
			const formData = new FormData();
			formData.append(property, value);

			// запускаем валидацию поля ввода потерявшего фокус
			const error = this.getError(formData, property);
			if (error.length == 0) return;
			// выводим текст ошибки
			this.showError(property, error);
		}

		getError(formData, property) {
			let error = '';
			// создаём литеральный объект validate
			// каждому свойству литерального объекта соответствует анонимная функция, в которой
			// длина значения поля, у которого атрибут 'name' равен 'property', сравнивается с 0,
			// а само значение - с соответствующим паттерном
			// если сравнение истинно, то переменной error присваивается текст ошибки
			const validate = {
				username: () => {
          if (formData.get('username').length == 0) {
						error = Form.errorMess[0];
					} else if (Form.patternName.test(formData.get('username')) == false) {
						error = Form.errorMess[1];
					}
					// if (formData.get('username').length == 0 || Form.patternName.test(formData.get('username')) == false) {
					// 	error = Form.errorMess[1];
					// }
				},
				usermail: () => {
					if (formData.get('usermail').length == 0) {
						error = Form.errorMess[2];
					} else if (Form.patternMail.test(formData.get('usermail')) == false) {
						error = Form.errorMess[3];
					}
				},
				userphone: () => {
					if (formData.get('userphone').length == 0) {
						error = Form.errorMess[4];
					} else if (Form.patternPhone.test(formData.get('userphone')) == false) {
						error = Form.errorMess[5];
					}
				},
				textmess: () => {
					if (formData.get('textmess').length == 0) {
						error = Form.errorMess[6];
					}
				}
			}

			if (property in validate) {
				// если после вызова анонимной функции validate[property]() переменной error
				// было присвоено какое-то значение, то это значение и возвращаем,
				// в противном случае значение error не изменится
				validate[property]();
			}
			return error;
		}

		showError(property, error) {
			// получаем объект элемента, в который введены ошибочные данные
			const el = this.form.querySelector(`[name=${property}]`);
			// с помощью DOM-навигации находим <span>, в который запишем текст ошибки
			const errorBox = Form.getElement(el);

			el.classList.add('form-control_error');
			// записываем текст ошибки в <span>
			errorBox.innerHTML = error;
			// делаем текст ошибки видимым
			errorBox.style.display = 'block';
		}

		cleanError(el) {
			// с помощью DOM-навигации находим <span>, в который записан текст ошибки
			const errorBox = Form.getElement(el);
			el.classList.remove('form-control_error');
			errorBox.removeAttribute('style');
			this.iserror = false;
		}

		sendFormData(formData) {
			let xhr = new XMLHttpRequest();
      const text__form = document.querySelector('.form__text');
      const list__form = document.querySelector('.form__list');
      const send__form = document.querySelector('.send__form');
			// указываем метод передачи данных, адрес php-скрипта, который эти данные
			// будет обрабатывать и способ отправки данных.
			// значение 'true' соответствует асинхронному запросу
			xhr.open('POST', '/sendmail.php', true);
			// xhr.onreadystatechange содержит обработчик события,
			// вызываемый когда происходит событие readystatechange
			xhr.onreadystatechange = () => {
				if (xhr.readyState === 4) {
					if (xhr.status === 200) {
						// здесь расположен код вашей callback-функции
						// например, она может выводить сообщение об успешной отправке письма
            
					} else {
						// здесь расположен код вашей callback-функции
						// например, она может выводить сообщение об ошибке
           
					}
				} else {
					// здесь расположен код вашей callback-функции
					// например, она может выводить сообщение об ошибке
				}
			}
			// отправляем данные формы
			xhr.send(formData);
      text__form.classList.toggle('form__text--delete');
      list__form.classList.toggle('form__list--delete');
      send__form.classList.toggle('send__form--active');
		}
	}

	// коллекция всех HTML форм на странице
	const forms = document.querySelectorAll('[name=feedback]');
	// если формы на странице отсутствуют, то прекращаем работу функции
	if (!forms) return;
	// перебираем полученную коллекцию элементов
	for (let form of forms) {
		// создаём экземпляр формы
		const f = new Form(form);
	}
})();

// ScrollUp //

$(function() {
    $('.scrollup').click(function() {
      $("html, body").animate({
        scrollTop:0
      },1000);
    })
  })
  $(window).scroll(function() {
    if ($(this).scrollTop()>200) {
      $('.scrollup').fadeIn();
    }
    else {
      $('.scrollup').fadeOut();
    }
});


// Switching Tabs //

const tabs = document.querySelectorAll(".tab-mobile__button");
const tabContent = document.querySelectorAll(".cards-value__block");

let tabNo = 0;
let contentNo = 0;

tabs.forEach((tab) => {
  tab.dataset.id = tabNo;
  tabNo++;
  tab.addEventListener("click", function () {
    tabs.forEach((tab) => {
      tab.classList.remove("tab-active");
      tab.classList.add("non-active");
    });
    this.classList.remove("tab-non-active");
    this.classList.add("tab-active");
    tabContent.forEach((content) => {
      content.classList.add("card-hidden");
      if (content.dataset.id === tab.dataset.id) {
          content.classList.remove("card-hidden");
      }
    });
  });
});

tabContent.forEach((content) => {
  content.dataset.id = contentNo;
  contentNo++;
});



// Filter Suits //

// получаем все кнопки для фильтрации
const filterSuitButtons = document.querySelectorAll(".js-filter-suits-buttons .filter-btn");

// проходимся по кнопкам через ццикл и получаем каждую отдельно
filterSuitButtons.forEach((button) => {

	// добавляем событие КЛИК на кадждую кнопку
	button.addEventListener("click", function (event) {
	
		// снова проходимся по всем кнопкам чтобы удалить класс
		filterSuitButtons.forEach((btn) => {
			btn.classList.remove('active')
		})

		// добавляем класс только на ту кнопку, на которуб кликнули
		this.classList.add('active');

		// получаем дата аттрибут из элетемна кнопки, по нему будем фильтровать
		let filter = this.dataset.filter;
		// получаем все фильтруемые карточки
		let cards = document.querySelectorAll('.suit');

		// проходимся по всем карточкам
		cards.forEach(function(card) {
			// если фильтр "ВСЕ"
			if (filter === 'all') {
				// удаляем все классы
				card.classList.remove('hide');
				card.classList.remove('show');
			} else { // если фильтр любой кроме "ВСЕ"
				// если у карточки в списке классов есть нужный класс
				if (card.classList.contains(filter + '__suit')) {
					// удаляем класс если он есть
					card.classList.remove('hide');
					card.classList.add('show');
				} else { // если карточка не фильтруемая
					//скрываем
					card.classList.add('hide');
					card.classList.remove('show');
				}
			}
		});
		
	}, true)
})


// Плавный переход по якорям //


var $page = $('html, body');
$('a[href*="#"]').click(function() {
    $page.animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 1000);
    return false;
});